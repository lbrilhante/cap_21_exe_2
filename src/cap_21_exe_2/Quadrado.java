/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cap_21_exe_2;

/**
 *
 * @author lucas
 */
public class Quadrado extends FormaGeometrica{
    //atributos
    private float lado;
    //construtor
    public Quadrado(float lado, String Perimetro, String Area) {
        super(Perimetro, Area);
        this.lado = lado;
    }
    //metodos
    public float getLado() {
        return lado;
    }

    public void setLado(float lado) {
        this.lado = lado;
    }
    
    
}
