/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cap_21_exe_2;

/**
 *
 * @author lucas
 */
public class TrianguloObtusangulo extends Triangulo{
    private int MaiorAngulo;

    public TrianguloObtusangulo(int MaiorAngulo, int base, int lado1, int lado2, String Perimetro, String Area) {
        super(base, lado1, lado2, Perimetro, Area);
        this.MaiorAngulo = MaiorAngulo;
    }

    public int getMaiorAngulo() {
        return MaiorAngulo;
    }

    public void setMaiorAngulo(int MaiorAngulo) {
        this.MaiorAngulo = MaiorAngulo;
    }
    
}
