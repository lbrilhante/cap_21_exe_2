/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cap_21_exe_2;

/**
 *
 * @author lucas
 */
public class TrianguloAcutangulo extends Triangulo{
    private float Angulos[];

    public TrianguloAcutangulo(float[] Angulos, int base, int lado1, int lado2, String Perimetro, String Area) {
        super(base, lado1, lado2, Perimetro, Area);
        this.Angulos = Angulos;
    }

    public float[] getAngulos() {
        return Angulos;
    }

    public void setAngulos(float[] Angulos) {
        this.Angulos = Angulos;
    }
    
}
