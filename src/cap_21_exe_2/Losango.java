/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cap_21_exe_2;

/**
 *
 * @author lucas
 */
public class Losango extends FormaGeometrica{
    //atributos
    private int diagonalMaior;
    private int diagonalMenor;
    //contrutor
    public Losango(int diagonalMaior, int diagonalMenor, String Perimetro, String Area) {
        super(Perimetro, Area);
        this.diagonalMaior = diagonalMaior;
        this.diagonalMenor = diagonalMenor;
    }
    //metodos
    public int getDiagonalMaior() {
        return diagonalMaior;
    }

    public void setDiagonalMaior(int diagonalMaior) {
        this.diagonalMaior = diagonalMaior;
    }

    public int getDiagonalMenor() {
        return diagonalMenor;
    }

    public void setDiagonalMenor(int diagonalMenor) {
        this.diagonalMenor = diagonalMenor;
    }
    

    
}
