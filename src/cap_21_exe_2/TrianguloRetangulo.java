/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cap_21_exe_2;

/**
 *
 * @author lucas
 */
public class TrianguloRetangulo extends Triangulo{
    private float angulos[];

    public TrianguloRetangulo(float[] angulos, int base, int lado1, int lado2, String Perimetro, String Area) {
        super(base, lado1, lado2, Perimetro, Area);
        this.angulos = angulos;
    }

    public float[] getAngulos() {
        return angulos;
    }

    public void setAngulos(float[] angulos) {
        this.angulos = angulos;
    }
    
}
