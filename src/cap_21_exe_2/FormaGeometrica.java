/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cap_21_exe_2;

/**
 *
 * @author lucas
 */
public class FormaGeometrica {
    //atributos
    public String Perimetro;
    public String Area;
    //contrutor
    public FormaGeometrica(String Perimetro, String Area) {
        this.Perimetro = Perimetro;
        this.Area = Area;
    }

    //metodos
    public String getPerimetro() {
        return Perimetro;
    }

    public void setPerimetro(String Perimetro) {
        this.Perimetro = Perimetro;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String Area) {
        this.Area = Area;
    }
    
}
