/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cap_21_exe_2;

/**
 *
 * @author lucas
 */
public class Retangulo extends FormaGeometrica{
    private int Base;
    private int altura;

    public Retangulo(int Base, int altura, String Perimetro, String Area) {
        super(Perimetro, Area);
        this.Base = Base;
        this.altura = altura;
    }

    public int getBase() {
        return Base;
    }

    public void setBase(int Base) {
        this.Base = Base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }
    
}
